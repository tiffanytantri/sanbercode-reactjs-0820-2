import React from 'react';
import './App.css';

function App() {
  return (
    <section style={{width: "40%", margin: "0 auto", border: "1px solid #000", borderRadius: "20px"}}>
      <div style={{textAlign: "center"}}>
        <h1>Form Pembelian Buah</h1>
      </div>
      <div style={{padding: "20px", paddingTop: "0"}}>
        <div>
          <b><label for="name" style={{display:"inline-block", width: "150px", marginTop: "1em"}}>Nama Pelanggan</label></b>
          <input type="text" id="name" name="name"></input><br></br>
        </div>

        <div style={{display: "inline-block", width: "150px", fontWeight: "bold", fontSize: "16px"}}>
            Daftar Item 
        </div>

        <div style={{display: "inline-block"}}>
          <input type="checkbox" name="semangka" value="semangka"></input>
          <label>Semangka</label><br></br>
          <input type="checkbox" name="jeruk" value="jeruk"></input>
          <label>Jeruk</label><br></br>
          <input type="checkbox" name="nanas" value="nanas"></input>
          <label>Nanas</label><br></br>
          <input type="checkbox" name="salak" value="salak"></input>
          <label>Salak</label><br></br>
          <input type="checkbox" name="anggur" value="anggur"></input>
          <label>Anggur</label><br></br>
        </div>
        <br></br>
        <button style={{background: "white", borderRadius: "20px", marginTop: "20px"}}>
          <a href="#" style={{textDecoration: "none", color: "black"}}>Kirim</a>
        </button>
      </div>
    </section>
  );
}

export default App;
